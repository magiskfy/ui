from time import sleep
from colorama import init, Fore, Style, Back
import os, json

default_dir = os.path.expanduser("~")
default_working_dir = os.path.join(default_dir, "Magiskfy")
config_file = os.path.join(default_dir, "magiskfy.json")

class instance():
    def __init__(self, config: list()):
        
        init(convert=True)
        print("\033[H\033[J")
        self.show_title()

        self.config = config
        
        if not self.check_config():
            self.get_config()

    def check_config(self):
        try:
            self.author = self.config['settings'][0]['author']
            self.working_dir = self.config['settings'][0]['files']
            return True
        except Exception:
            return False
        
    def get_config(self):
        self.author = input(' - Magiskfy config file was not found or is corrupted, please enter the default author: ')
        self.working_dir = input(" - Enter Magiskfy's installation path (leave blank to use {}): ".format(default_working_dir))
        if not self.working_dir:
            self.working_dir = default_working_dir

    def show_title(self):
        #Title
        print("\n\n -   " + Fore.GREEN + Style.BRIGHT + "[Magiskfy Script]" + Style.RESET_ALL + "   - \n")
        sleep(0.1) #gimmick

    def show_menu(self, saved_modules: list()):
        selected = None
        counter = 0
        if not saved_modules:
            print("\n\n" + Fore.RED + " - No saved modules found, create a new one. \n" + Style.RESET_ALL)
            selected = "*1"
        else:
            print("\033[H\033[J")
            self.show_title()
            for module in saved_modules:
                sleep(0.05)
                print("  " + str(counter+1) + " | " + module + ".")
                counter += 1
            print("\n - Options - \n")           
            print("  *1 | Create a new module from input folder.")
            print("  *2 | Change the author for the modules.")
            print("  *3 | To exit the script.")
            selected = input("\n - Select a saved module to update it or select an option: ")

            error = True
            while error:
                if selected == "*1" or selected == "*2" or selected == "*3":
                    error = False
                    break
                else:
                    try:
                        selected = int(selected)
                        if selected <= len(saved_modules):
                            error = False
                            break
                        else:
                            error = True
                    except Exception:
                        error = True
                selected = input(" - Enter a valid option: ")                  
        return selected

    def get_selection(self, saved_modules: list()):
        selected = self.show_menu(saved_modules)

        if selected == "*1": selected = "create"
        if selected == "*2": selected = "change_author"
        if selected == "*3": selected = "exit"
        
        return selected

    def change_author(self):
        new_author = input("\n - Enter the new default author for future modules ({}): ".format(self.author))
        return new_author

    def show_error(self, error):
        print("\n" + Fore.RED + error + Style.RESET_ALL)
    
    def show_message(self, message):
        print("\n" + Fore.GREEN + message + Style.RESET_ALL)

    def verify_string(self, string, name, spaces):
        error = True
        chars = set('\/:*?"<>|')
        if spaces == False:
            chars.add(" ")
        while error:
            if any((c in chars) for c in string) or string == "":
                error = True
                if spaces == False:
                    string = input(' * Enter a valid ' + name +  ' without (\ / : * ? " <> |): ')
                else:
                    string = input(' * Enter a valid ' + name +  ' without (\ / : * ? " <> |) and no spaces: ')
            else:
                error = False
        return string

    def create_module(self):
        print("\n - Creating a new module.\n")
        data = list()
        module_id = input(" * Enter the module's ID (no spaces): ")
        module_id = self.verify_string(module_id, "Module ID", False)

        module_name = input(" * Enter the module's name: ")
        module_name = self.verify_string(module_name, "module name", True)

        module_version = input(" * Enter the module's version: ")
        module_version = self.verify_string(module_version, "module version", True)

        version_code = input(" * Enter the module's version code (only integer, no spaces): ")
        error = True
        while error:
            try:
                version_code = int(version_code)
                break
            except ValueError:
                version_code = input(" * Enter a valid version code (only numbers, no spaces): ")
                error = True

        description = input(" * Enter the module's description: ")

        replace_path = input("\n * Enter the path that needs to replace\n * Example = /system/priv-app/Launcher3\n * Leave it blank to not replace anything: ")

        data.append(module_id)
        data.append(module_name)
        data.append(module_version)
        data.append(version_code)
        data.append(description)
        data.append(replace_path)

        return data

    def update_module(self, old_version):
        new_version = input("\n - Enter the new version ({}): ".format(old_version))
        return new_version

